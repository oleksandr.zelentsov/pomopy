#!/usr/bin/env python3
from argparse import ArgumentParser
from dataclasses import dataclass, field
from datetime import timedelta
from os.path import expanduser
from time import sleep


@dataclass
class PomodoroTimer:
    work_time: timedelta
    short_break_time: timedelta
    long_break_time: timedelta
    sessions: int = 4
    progress_resolution: int = 1
    sessions_passed: int = field(init=False, default=0)

    def wait(self, amount: timedelta, msg: str):
        self.start_progress()
        self.update_progress(amount, msg)
        for seconds in reversed(range(0, int(amount.total_seconds() + 1), self.progress_resolution)):
            sleep(self.progress_resolution)
            self.update_progress(timedelta(seconds=seconds), msg)

        self.finish_progress()

    def work_session(self):
        self.wait(self.work_time, 'Work session')
        self.sessions_passed += 1

    def work_break(self):
        self.wait(self.short_break_time, 'Have a short break')

    def long_work_break(self):
        self.wait(self.long_break_time, 'Have a long break')

    @staticmethod
    def start_progress():
        pass

    @staticmethod
    def update_progress(time_to_display: timedelta, msg: str):
        minutes, seconds = divmod(int(time_to_display.total_seconds()), 60)
        print(f'\r{msg}: {minutes:02d}:{seconds:02d}', end='')

    @staticmethod
    def finish_progress():
        pass

    def print_results(self):
        print()
        print(f'sessions done: {self.sessions_passed}')
        print(f'worked time: {self.work_time * self.sessions_passed}')
        print('quitting...')

    def __call__(self):
        try:
            while True:
                for i in range(self.sessions):
                    if i != 0:
                        self.work_break()
                    self.work_session()
                self.long_work_break()
        except KeyboardInterrupt:
            self.print_results()
            return


class EnvironmentBasedPomodoroTimer(PomodoroTimer):
    environment_file_path = expanduser('~/.pomorc.py')
    hooks = [
        'on_work_session',
        'on_short_break',
        'on_long_break',
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hook_namespace = {}

        try:
            with open(self.environment_file_path, 'r') as fp:
                module = compile(''.join(fp.readlines()), filename=self.environment_file_path, mode='exec')

            exec(module, self.hook_namespace)
        except (ValueError, SyntaxError, FileNotFoundError):
            self.hook_namespace = {}

        for hook in self.hooks:
            if self.hook_namespace.get(hook) is None:
                self.hook_namespace.update({
                    hook: lambda t: None,
                })

    def work_session(self):
        self.hook_namespace['on_work_session'](self)
        super().work_session()

    def work_break(self):
        self.hook_namespace['on_short_break'](self)
        super().work_break()

    def long_work_break(self):
        self.hook_namespace['on_long_break'](self)
        super().long_work_break()


def get_args(arguments=None):
    parser = ArgumentParser()

    parser.add_argument('-w', '--work-time', type=int, default=25, help='work time in minutes')
    parser.add_argument('-b', '--break-time', type=int, default=5, help='break time in minutes')
    parser.add_argument('-l', '--long-break', type=int, default=15, help='long break time in minutes')
    parser.add_argument('-c', '--work-sessions', type=int, default=4, help='amount of work sessions until long '
                                                                           'break will start')

    return parser.parse_args(arguments)


def main():
    args = get_args()
    work_time = timedelta(minutes=args.work_time)
    break_time = timedelta(minutes=args.break_time)
    long_break_time = timedelta(minutes=args.long_break)
    timer = EnvironmentBasedPomodoroTimer(work_time, break_time, long_break_time, args.work_sessions)
    timer()


if __name__ == '__main__':
    main()
