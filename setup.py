import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pomopy",
    version="0.1.1",
    author="Oleksandr Zelentsov",
    author_email="oleksandrzelentsov@gmail.com",
    description="A pomodoro timer",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/oleksandr.zelentsov/pomopy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: POSIX :: Linux",
    ],
    entry_points={
        'console_scripts': [
            'pomo=pomopy.pomo:main',
        ],
    },
)
